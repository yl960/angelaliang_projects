# Efficient Frontier

### Description

- **Goal**: In this class project, I used C++ to find optimizing multi-asset portfolio for each level of expeted return based on finding the optimal weights to achieve minimum volatility for each return
- **Language and Methods Used**: C++, Eigen package (MatrixXd)
- **Challenges & Learnings**: The key challenge of the project is to deal with the matrix calculation, and I chose to use the Eigen package as it is known for its high performance and optimized for speed. I thought deeply about corner cases, involving the actual numbers and matrices that could pose problems and tested these scenarios specifically.

### Prerequisites

- A modern C++ compiler (supporting C++17 standards)
- The Eigen library installed on your system
- Command-line terminal or equivalent

## To run the program:
For unrestricted case, run: ./efficient_frontier universe.csv correlation.csv
- to view output: ./efficient_frontier universe.csv correlation.csv > unrestriced_output.csv
For restricted case, run: ./efficient_frontier -r universe.csv correlation.csv
- to view output: ./efficient_frontier -r universe.csv correlation.csv > restricted_output.csv

### Reference
- View the assignment instructions in `efficient_frontier.pdf`
- `eff_frontier_optimization_approach.pdf` presents methods used to solve for the efficient frontier.
- `Frontiers-EmmaRasiel.pdf` provides additional background material on
Portfolio Theory and the efficient frontier. 

### Contact

For any inquiries or contributions, please contact me at angela.liang@duke.edu