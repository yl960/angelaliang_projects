#include "efficient_frontier.cpp"
// #include "efficient_frontier.hpp"
#include <assert.h>
#include <ostream>
#include <iostream>
#include <fstream>
#include <string>
#include <limits>  // For numeric_limits
#include <iomanip> // For setw, setprecision, and fixed
#include <cmath>
#include <vector>
#include <math.h>
#include <filesystem>

using namespace std;

std::ifstream checkFile(const string &filename)
{

      if (std::filesystem::path(filename).extension() != ".csv")
      {
            // throw std::invalid_argument("Input file must be a csv file");
            std::cerr << "Input file must be a csv file" << std::endl;
            exit(EXIT_FAILURE);
      }
      std::ifstream in(filename); // read in the file to string, output string

      if (!in.is_open())
      {
            // throw std::runtime_error("Unable to open " + filename);
            std::cerr << "Unable to open " << filename << std::endl;
            exit(EXIT_FAILURE);
      }

      if (!std::filesystem::exists(filename))
      {
            std::cerr << "Error: File '" << filename << "' does not exist." << std::endl;
            exit(EXIT_FAILURE);
      }

      return in;
}

int main(int argc, char *argv[])
{
      // if (argc < 3)
      // {
      //       std::cerr << "Usage: " << argv[0] << " <filename>" << std::endl;
      //       return EXIT_FAILURE;
      // }

      if (argc != 3 && argc != 4)
      {
            std::cerr << "Usage: incorrect number of arguments" << std::endl;
            return EXIT_FAILURE;
      }
      if (argc == 3)
      {

            std::string filename1 = argv[1];
            std::string filename2 = argv[2];
            checkFile(filename1);
            checkFile(filename2);
            std::vector<std::vector<string>> univMatrix = parseUniverse(filename1);
            MatrixXd corrMatrix = parseCorrelation(filename2, univMatrix.size());
            MatrixXd covMatrix = unrestrictedCaseCovMatrix(univMatrix, corrMatrix);
            MatrixXd newunivMatrix = convertNewUnivMatrix(univMatrix);
            MatrixXd unrestrictedMatrix = unrestrictedCase(univMatrix, corrMatrix, newunivMatrix);
            MatrixXd portVol = portfolioVolatility(unrestrictedMatrix, covMatrix);

            // std::cout << "Here is a solution x to the equation Ax=b (unrestricted case):" << endl;
            std::cout << "ROR,volatility" << endl;
            for (int i = 0; i < 26; i++)
            {
                  std::cout << std::fixed << std::setprecision(1) << portVol(i, 0) * 100 << "%," << std::fixed << std::setprecision(2) << portVol(i, 1) * 100 << "%" << endl;
            }
      }
      if (argc == 4)
      {

            if (std::string(argv[1]) != "-r")
            {
                  std::cerr << "Usage: incorrect 2nd argument" << std::endl;
                  return EXIT_FAILURE;
            }

            std::string filename1 = argv[2];
            std::string filename2 = argv[3];
            checkFile(filename1);
            checkFile(filename2);
            std::vector<std::vector<string>> univMatrix = parseUniverse(filename1);
            MatrixXd corrMatrix = parseCorrelation(filename2, univMatrix.size());
            MatrixXd covMatrix = unrestrictedCaseCovMatrix(univMatrix, corrMatrix);
            MatrixXd newunivMatrix = convertNewUnivMatrix(univMatrix);

            // std::cout << "Here is a solution x to the equation Ax=b (restricted case):" << endl;
            std::cout << "ROR,volatility" << endl;
            MatrixXd restrictedMatrix;
            for (double rate = 0.01; rate < 0.27; rate += 0.01)
            {
                  // assign values (0,0) and (0,1)

                  restrictedMatrix = restrictedCase(corrMatrix, newunivMatrix, rate);
                  std::cout << std::fixed << std::setprecision(1) << restrictedMatrix(0, 0) * 100 << "%"
                            << "," << std::fixed << std::setprecision(2) << restrictedMatrix(0, 1) * 100 << "%" << endl;
            }
      }

      return 0;
}
