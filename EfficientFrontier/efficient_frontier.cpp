#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <limits>  // For numeric_limits
#include <iomanip> // For setw, setprecision, and fixed
#include <cmath>
#include <vector>
#include <math.h>
#include <typeinfo>
#include <iostream>
#include <filesystem>
#include "Eigen/Eigen/Dense"
#include "Eigen/Eigen/SVD"

using Eigen::MatrixXd;
using namespace Eigen;

using namespace std;

vector<string> split(string line){
      istringstream ss(line);
      vector<string> values;
      string item;
      while (getline(ss, item, ',')){
            if (item.empty()){
                  continue;
            }
            values.push_back(item);
      }
      return values;
}

std::vector<std::vector<string>> parseUniverse(const string filename){
      vector<vector<string>> matrix;
      std::ifstream file;
      file.open(filename);
      if (!file.is_open()){
            std::cerr << "Error opening the file" << std::endl;
            exit(EXIT_FAILURE);
      }
      std::string line;
      while (getline(file, line, '\n')){
            if (!line.empty()){
                  vector<string> row = split(line);
                  // Check the number of columns
                  if (row.size() != 3){
                        std::cerr << "Error: Invalid number of columns in the universe file." << std::endl;
                        exit(EXIT_FAILURE);
                  }
                  // Check the data types of columns, 2nd and 3rd columns are numeric
                  try{
                        std::stod(row[1]);
                        std::stod(row[2]);
                  }
                  catch (const std::invalid_argument &e){
                        std::cerr << "Error: Numeric columns in the universe file must contain numeric values." << std::endl;
                        exit(EXIT_FAILURE);
                  }
                  matrix.push_back(row);
            }
            else{
                  std::cerr << "Error: Invalid data in the file." << std::endl;
                  exit(EXIT_FAILURE);
            }
      }
      return matrix;
}

MatrixXd parseCorrelation(const string filename, int numAssets){
      std::ifstream file;
      file.open(filename);
      if (!file.is_open()){
            std::cerr << "Error opening the file" << std::endl;
            exit(EXIT_FAILURE);
      }
      std::string line;
      vector<vector<string>> matrix;
      while (getline(file, line, '\n')){
            if (!line.empty()){
                  vector<string> row = split(line);
                  // Check the data types of columns - should be numeric
                  for (const string &value : row){
                        try{
                              std::stod(value); // Check if each value is numeric
                        }
                        catch (const std::invalid_argument &e){
                              std::cerr << "Error: Numeric columns in the correlation file must contain numeric values." << std::endl;
                              exit(EXIT_FAILURE);
                        }
                  }
                  matrix.push_back(row);
            }
            else{
                  std::cerr << "Error: Invalid data in the file." << std::endl;
                  exit(EXIT_FAILURE);
            }
      }
      int nrows = matrix.size();
      int ncols = matrix[0].size();
      // Check if the nrows = ncols;
      if (ncols != nrows){
            std::cerr << "Error: The correlation matrix must be square." << std::endl;
            exit(EXIT_FAILURE);
      }
      // Check if the number of rows in the correlation matrix matches the number of columns in the universe file
      if (nrows != numAssets){
            std::cerr << "Error: The number of rows in the correlation matrix must match the number of columns in the universe file." << std::endl;
            exit(EXIT_FAILURE);
      }
      MatrixXd corrMatrix(nrows, nrows);
      for (int row = 0; row < nrows; row++){
            for (int col = 0; col < ncols; col++){
                  corrMatrix(row, col) = std::stod(matrix[row][col]);
            }
      }
      for (int row = 0; row < nrows; row++){
            if(corrMatrix(row, row) != 1.0){
                  std::cerr << "Diagonal number must be 1.0 for correlation" << std::endl;
                  exit(EXIT_FAILURE);
            };
      }
      return corrMatrix;
}

MatrixXd unrestrictedCaseCovMatrix(vector<vector<string>> univMatrix, MatrixXd corrMatrix){

      int nrows = univMatrix.size();
      MatrixXd newunivMatrix(nrows, 2); // 7x2
      for (int row = 0; row < nrows; row++){
            newunivMatrix(row, 0) = std::stod(univMatrix[row][1]);
            newunivMatrix(row, 1) = std::stod(univMatrix[row][2]);
      }
      int numAssets = univMatrix.size();
      MatrixXd covMatrix(numAssets, numAssets);
      for (int i = 0; i < numAssets; i++){
            for (int j = 0; j < numAssets; j++){
                  covMatrix(i, j) = corrMatrix(i, j) * newunivMatrix(i, 1) * newunivMatrix(j, 1);
            }
      }
      return covMatrix;
}

MatrixXd convertNewUnivMatrix(vector<vector<string>> univMatrix){
      int nrows = univMatrix.size();
      MatrixXd newunivMatrix(nrows, 2); // 7x2
      for (int row = 0; row < nrows; row++){
            newunivMatrix(row, 0) = std::stod(univMatrix[row][1]);
            newunivMatrix(row, 1) = std::stod(univMatrix[row][2]);
      }
      return newunivMatrix;
}

MatrixXd unrestrictedCase(vector<vector<string>> univMatrix, MatrixXd corrMatrix, MatrixXd newunivMatrix){
      int numAssets = univMatrix.size(); // to be modified
      MatrixXd covMatrix(numAssets, numAssets);
      for (int i = 0; i < numAssets; i++){
            for (int j = 0; j < numAssets; j++){
                  covMatrix(i, j) = corrMatrix(i, j) * newunivMatrix(i, 1) * newunivMatrix(j, 1);
            }
      }
      MatrixXd AA(2, numAssets);
      MatrixXd AT(numAssets, 2);
      for (int i = 0; i < numAssets; i++){
            AA(0, i) = 1.0;
            AA(1, i) = newunivMatrix(i, 0); // (7x2)
      }

      for (int i = 0; i < numAssets; i++){
            AT(i, 0) = AA(0, i);
            AT(i, 1) = AA(1, i);
      }
      MatrixXd B(numAssets + 2, 26); // 9 in total
      vector<double> port_rr;
      for (int r = 1.0; r < 27.0; r++){
            double new_r = r / 100.0;
            port_rr.push_back(new_r);
      }
      for (int i = 0; i < numAssets; i++){
            for (int r = 1; r < 27; r++){
                  B(i, r - 1) = 0.0;
            }
      }
      for (int r = 0; r < 26; r++){
            B(numAssets, r) = 1.0;            // starting from #7, 7 and 8 which is 8th and 9th to be 1 and port_rr
            B(numAssets + 1, r) = port_rr[r]; // port_rr from 1% to 26%
      }
      MatrixXd BigMatrix(numAssets + 2, numAssets + 2);
      for (int i = 0; i < numAssets; i++){
            for (int j = 0; j < numAssets; j++){
                  BigMatrix(i, j) = covMatrix(i, j);
            }
      }
      for (int i = numAssets; i < numAssets + 2; i++){
            for (int j = 0; j < numAssets; j++){
                  BigMatrix(i, j) = AA(i - numAssets, j);
            }
      }
      for (int i = 0; i < numAssets; i++){
            for (int j = numAssets; j < numAssets + 2; j++){
                  BigMatrix(i, j) = AT(i, j - numAssets);
            }
      }
      for (int i = numAssets; i < numAssets + 2; i++){
            for (int j = numAssets; j < numAssets + 2; j++){
                  BigMatrix(i, j) = 0.0;
            }
      }
      MatrixXd x(numAssets + 2, 1);
      MatrixXd allSoln(numAssets + 2, 26);
      for (int j = 0; j < 26; j++){
            x = BigMatrix.colPivHouseholderQr().solve(B.col(j));
            allSoln.col(j) = x;
      }
      return allSoln;
}

MatrixXd portfolioVolatility(MatrixXd unrestrictedMatrix, MatrixXd covMatrix){
      int numAssets = covMatrix.col(0).size();
      MatrixXd vol(26, 2);
      for (int r = 0; r < 26; r++){
            double bigsum = 0.0;
            for (int i = 0; i < numAssets; i++){
                  double smallsum = 0.0;
                  for (int j = 0; j < numAssets; j++){
                        double prodt = unrestrictedMatrix(i, r) * unrestrictedMatrix(j, r) * covMatrix(i, j);
                        smallsum += prodt;
                  }
                  bigsum += smallsum;
            }
            vol(r, 1) = std::sqrt(bigsum);
      }
      vector<double> port_rr;
      for (int r = 1; r < 27; r++){
            double new_r = r / 100.0;
            port_rr.push_back(new_r);
      }
      for (int r = 0; r < 26; r++){
            vol(r, 0) = port_rr[r];
      }
      return vol;
}

MatrixXd restrictedCaseSolve(MatrixXd updatedMatrix, MatrixXd updatedB){
      int rows = updatedMatrix.col(0).size(); // 9x9;
      MatrixXd x(rows, 1);
      x = updatedMatrix.colPivHouseholderQr().solve(updatedB);
      return x;
}

vector<int> countNegative(MatrixXd columnMatrix, int numAssets){
      vector<int> storenegative;
      for (int row = 0; row < numAssets; row++){
            if (columnMatrix(row, 0) < -1e-12){
                  storenegative.push_back(row);
            }
      }
      return storenegative; // countnegative = storenegative.size();
}

MatrixXd updateMatrix(MatrixXd oldmatrix, int countnegative, vector<int> storenegative){
      int oldrows = oldmatrix.rows(); // 7
      int oldcols = oldmatrix.cols(); // 7
      MatrixXd newmatrix = MatrixXd::Zero(oldrows + countnegative, oldcols + countnegative);
      for (int i = 0; i < oldrows; i++){
            for (int j = 0; j < oldcols; j++){
                  newmatrix(i, j) = oldmatrix(i, j);
            }
      }
      for (int i = 0; i < countnegative; i++){
            newmatrix(oldrows + i, storenegative[i]) = 1.0; //(oldrows =7)+(i=0) = 7, start at the 8th row
            newmatrix(storenegative[i], oldrows + i) = 1.0; // just a swap of row/col
      }
      return newmatrix; // this will return an updated matrix, then feed into the unrestrictedcase again to produce weight
}

MatrixXd updateB(MatrixXd oldB, int countnegative, vector<int> storenegative){
      int oldrows = oldB.rows(); // 7
      MatrixXd newB = MatrixXd::Zero(oldrows + countnegative, 1);
      for (int i = 0; i < oldrows; i++){
            newB(i, 0) = oldB(i, 0);
      }
      return newB; // this will return an updated matrix, then feed into the unrestrictedcase again to produce weight
}

double portfolioRestrictedVolatility(MatrixXd updatedColumnWeights, MatrixXd covMatrix){
      int numAssets = covMatrix.col(0).size(); // find the solution size;
      double bigsum = 0.0;
      for (int i = 0; i < numAssets; i++){
            double smallsum = 0.0;
            for (int j = 0; j < numAssets; j++){
                  double prodt = updatedColumnWeights(i, 0) * updatedColumnWeights(j, 0) * covMatrix(i, j);
                  smallsum += prodt;
            }
            bigsum += smallsum;
      }
      double result = std::sqrt(bigsum);
      return result; // updated vol one line each time;
}

MatrixXd restrictedCase(MatrixXd corrMatrix, MatrixXd newunivMatrix, double returnrate){
      int numAssets = newunivMatrix.col(0).size(); // to be modified
      MatrixXd covMatrix = MatrixXd::Zero(numAssets, numAssets);
      for (int i = 0; i < numAssets; i++){
            for (int j = 0; j < numAssets; j++){
                  covMatrix(i, j) = corrMatrix(i, j) * newunivMatrix(i, 1) * newunivMatrix(j, 1);
            }
      }
      MatrixXd AA = MatrixXd::Zero(2, numAssets);
      MatrixXd AT = MatrixXd::Zero(numAssets, 2);
      for (int i = 0; i < numAssets; i++){
            AA(0, i) = 1.0;
            AA(1, i) = newunivMatrix(i, 0); // (7x2)
      }
      // after creating A, create A transpose
      for (int i = 0; i < numAssets; i++){
            AT(i, 0) = AA(0, i);
            AT(i, 1) = AA(1, i);
      }
      MatrixXd B = MatrixXd::Zero(numAssets + 2, 1); // 9 in total
      for (int i = 0; i < numAssets; i++){

            B(i, 0) = 0.0;
      }
      B(numAssets, 0) = 1.0;            // starting from #7, 7 and 8 which is 8th and 9th to be 1 and port_rr
      B(numAssets + 1, 0) = returnrate; // port_rr from 1% to 26%
      MatrixXd BigMatrix = MatrixXd::Zero(numAssets + 2, numAssets + 2);
      for (int i = 0; i < numAssets; i++){
            for (int j = 0; j < numAssets; j++){
                  BigMatrix(i, j) = covMatrix(i, j);
            }
      }
      for (int i = numAssets; i < numAssets + 2; i++){
            for (int j = 0; j < numAssets; j++){
                  BigMatrix(i, j) = AA(i - numAssets, j);
            }
      }
      for (int i = 0; i < numAssets; i++){
            for (int j = numAssets; j < numAssets + 2; j++){
                  BigMatrix(i, j) = AT(i, j - numAssets);
            }
      }
      for (int i = numAssets; i < numAssets + 2; i++){
            for (int j = numAssets; j < numAssets + 2; j++){
                  BigMatrix(i, j) = 0.0;
            }
      }
      MatrixXd originalMatrixWeights = restrictedCaseSolve(BigMatrix, B);
      int inf_num = 0;
      MatrixXd updatedMatrix;
      MatrixXd updatedB;
      MatrixXd updatedPortfolioVolatility(1, 2);
      for (;; inf_num++){
            vector<int> storenegative = countNegative(originalMatrixWeights, numAssets);
            int countNewNegative = (countNegative(originalMatrixWeights, numAssets)).size();
            if (countNewNegative > 0){
                  if (inf_num == 0){
                        updatedMatrix = BigMatrix;
                        updatedB = B;
                  }
                  else{
                        MatrixXd newupdatedMatrix = updateMatrix(updatedMatrix, countNewNegative, storenegative);
                        int M_num_row = newupdatedMatrix.rows();
                        int M_num_col = newupdatedMatrix.cols();
                        updatedMatrix.resize(M_num_row, M_num_col);
                        for (int i = 0; i < M_num_row; i++){
                              for (int j = 0; j < M_num_col; j++){
                                    updatedMatrix(i, j) = newupdatedMatrix(i, j);
                              }
                        }
                        MatrixXd newupdatedB = updateB(updatedB, countNewNegative, storenegative);
                        int B_num_row = newupdatedB.rows();
                        int B_num_col = newupdatedB.cols();
                        updatedB.resize(B_num_row, B_num_col);
                        for (int i = 0; i < B_num_row; i++){
                              for (int j = 0; j < B_num_col; j++){
                                    updatedB(i, j) = newupdatedB(i, j);
                              }
                        }
                  }
                  MatrixXd newMatrixWeights = restrictedCaseSolve(updatedMatrix, updatedB);
                  int new_row = newMatrixWeights.rows();
                  int new_col = newMatrixWeights.cols();
                  originalMatrixWeights.resize(new_row, new_col);
                  for (int i = 0; i < new_row; i++){
                        for (int j = 0; j < new_col; j++){
                              originalMatrixWeights(i, j) = newMatrixWeights(i, j);
                        }
                  }
            }
            if (countNewNegative == 0){
                  updatedPortfolioVolatility(0, 0) = returnrate;
                  updatedPortfolioVolatility(0, 1) = portfolioRestrictedVolatility(originalMatrixWeights, covMatrix);
                  break;
            }
      }
      return updatedPortfolioVolatility;
}
