# Phase I - Efficiency vs Accuracy Trade-offs in Signatures Method

## Overview

In Phase I of my project, I conducted an analysis to understand the trade-offs between efficiency and accuracy when applying the signatures method. This phase focuses on evaluating how well the signatures method performs, especially in terms of accuracy, under varying sample sizes.

## Key Findings

- **Sample Size Influence**: A significant observation is that smaller sample sizes can lead to overfitting problems, thus limiting the accuracy of the signatures method. This finding underscores the importance of considering sample size in the application of the method.

## Project Structure

- `Signatures_Accuracy.ipynb`: A Jupyter notebook containing my detailed analysis, including experiments and findings.
- `Signature Methods Presentation.pdf`: A presentation that summarizes the methodologies, key findings, and the broader implications of the study.
- `Signatures Method_Classification Accuracy Report.pdf`: An in-depth report detailing the observed trade-offs between classification accuracy and efficiency when using the signatures method.

## Methodology

I employed both theoretical and practical approaches to investigate the efficiency and accuracy trade-offs of the signatures method. This includes:

1. **Designing Experiments**: Setting up experiments to assess the performance of the signatures method across different sample sizes.
2. **Data Analysis**: Carefully analyzing the impact of these variables on the method's performance.
3. **Interpreting Results**: Discussing the practical implications of my findings for the application of the signatures method in various domains.

## Usage

To explore my analysis and experiments:

1. Ensure Jupyter Notebook is installed on your system.
2. Open the `Signatures_Accuracy.ipynb` notebook.
3. Execute the cells in sequence to follow my investigation into how sample size affects the accuracy and efficiency of the signatures method.

## Necessary Python Libraries

To run the `Signatures_Accuracy.ipynb` notebook, you will need the following Python libraries:

- `esig`: Specifically the `tosig` module for signature computation.
- `signatory`: For computing signatures of streams of data.
- `tensorflow`, `math`, `matplotlib.pyplot`,`numpy`,`pandas`,`random`,`sys`
- `torch`: PyTorch, for tensor operations and deep learning.


## Contact

If you have any questions or need more information about this phase of the project, feel free to contact me at angela.liang@duke.edu. 

---

*This phase of research is a part of my continuous effort to explore, understand, and utilize the capabilities and limitations of the signatures method in data analysis.*
