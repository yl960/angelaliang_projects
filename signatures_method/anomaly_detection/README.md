## Description ## 
A Kaggle anomaly-detection dataset is used to demonstrate the practical application of the signatures method. The project was originally done on jupyter notebook pulling datasets saved in a google drive. The dataset is now provided in the `anomaly_dataset` file for easy reference. 

## Project Structure
- `Signatures in Anomaly Detection.ipynb`: Jupyter notebook containing the core analysis, including data processing, application of signature methods, and anomaly detection algorithms.
- `Signatures Method Project Poster.pdf`: A poster summarizing the project's methodology, findings, and significance.

## Methodology
The project employs the signature method for feature extraction from time-series data, followed by the implementation of anomaly detection algorithms. The process involves:

1. **Data Preprocessing**: Preparing the time-series data for analysis.
2. **Feature Extraction**: Using signature methods to extract meaningful features from the data.
3. **Anomaly Detection**: Applying statistical techniques to identify anomalies in the data.

## Results and Findings
The results indicate that signature methods are effective in capturing the complex patterns in time-series data, thus facilitating more accurate anomaly detection compared to traditional methods. Detailed results and analysis are available in the poster.

## Usage
To run the analysis:

1. Ensure you have Jupyter Notebook installed.
2. Open the `Signatures in Anomaly Detection.ipynb` notebook.
3. Run the cells sequentially to see the data processing, signature feature extraction, and anomaly detection steps.

## Requirements
- Python 3.x
- Jupyter Notebook
- Necessary Python libraries: 
      - `esig`: Specifically the `tosig` module for signature computation.
      - `signatory`: For computing signatures of streams of data.
      - `tensorflow`, `math`, `matplotlib.pyplot`,`numpy`,`pandas`,`random`,`sys`
      - `torch`: PyTorch, for tensor operations and deep learning.

## Contact
For more information or queries regarding this project, feel free to contact me at angela.liang@duke.edu. 
