# Multivariant and Regression

## Overview

This project showcases my solutions for a previous coursework assignment on multivariant and regression, which involved various statistical analyses and modeling techniques. The assignment comprises three main problems, each focusing on different aspects of statistical data analysis. 

## Table of Contents

1. [Problem 1: Testing Statistical Package Biasness](https://gitlab.oit.duke.edu/yl960/angelaliang_projects/-/blob/main/MultivariantAndRegression/1.Statistical%20Package%20Biasness.ipynb)
2. [Problem 2: Linear Regression Analysis](https://gitlab.oit.duke.edu/yl960/angelaliang_projects/-/blob/main/MultivariantAndRegression/2.OLS%20vs%20MLE.ipynb)
3. [Problem 3: Time Series Analysis](https://gitlab.oit.duke.edu/yl960/angelaliang_projects/-/blob/main/MultivariantAndRegression/3.Time%20Series%20Analysis.ipynb?ref_type=heads)

## Problem 1: Testing Statistical Package Biasness

### Objective

The objective was to test the biasness of a statistical package. This involved computing the first four moments (mean, variance, skewness, and kurtosis) of a given sample and comparing these values with those obtained using numpy and scipy packages. A t-test was performed to assess the significance of any differences.

### Approach

- Calculated the first four moments using a custom function `first4Moments`.
- Compared these results with those obtained using numpy's mean and variance functions and scipy's skew and kurtosis functions.
- Performed t-tests for each moment to test the hypothesis of difference.
- Set a significance level (alpha) at 0.05 for hypothesis testing.

### Conclusion

The t-test results indicated that the statistical package was unbiased for mean calculation but biased for variance, skewness, and kurtosis calculations.

## Problem 2: Linear Regression Analysis

### Objective

This problem aimed to estimate parameters of a linear regression model using both Ordinary Least Squares (OLS) and Maximum Likelihood Estimation (MLE) methods, and compare their results.

### Approach

- Performed OLS regression using the statsmodels library.
- Implemented a custom negative log-likelihood function for MLE.
- Estimated regression coefficients and error variance using both OLS and MLE.
- Compared results from both methods.

### Conclusion

The results indicated that estimates from both OLS and MLE were largely consistent, suggesting robustness in these estimation methods.

## Problem 3: Time Series Analysis

### Objective

The objective was to find the best model for a given time series data set, exploring various combinations of ARIMA models.

### Approach

- Parsed the data and split it into training and testing sets.
- Used the ARIMA model from the statsmodels package.
- Fitted various combinations of ARIMA models and compared them based on their AIC and BIC values.

### Conclusion

Based on the lowest AIC and BIC values, AR(3) was determined to be the best model for the time series data.

## Technologies Used

- Python
- Pandas
- NumPy
- SciPy
- Statsmodels

## Author

Angela Liang

## Acknowledgements

Special thanks to my instructor and peers for their invaluable feedback and support throughout this project.
