# AngelaLiang_Projects



## Hello

Welcome to my personal project portfolio on GitLab! Here you'll find a curated collection of my work showcasing my skills in business and data analytics, software development, and more. This portfolio is a living document and is regularly updated with new projects and learnings, so be sure to check back often!


## About Me

I'm Yuyue (Angela) Liang, a first-year Master's student in Financial Technology at Duke Pratt School of Engineering, with a passion for leveraging data to drive decision making. With a background in B.S. in Financial Mathematics & Statistics, and B.A. Economics Accounting at UC Santa Barbara, I have developed a diverse set of skills that I enjoy applying to practical and challenging problems. 

## Projects 

Below is a list of projects included in this portfolio:

### [Dating App Study](https://gitlab.oit.duke.edu/yl960/angelaliang_projects/-/blob/main/dating_app_study)
- **Description**: This study, part of the Curiosity Cup 2024, delved into the factors that influence the success and attractiveness of dating profiles on the Lovoo platform. Using a dataset of 4015 female user profiles, I concentrated on metrics such as profile visits and engagements to determine what makes a profile stand out. The goal was to enhance user experience by providing actionable insights for profile optimization.
- **Tools and Techniques**: SAS for data cleansing, Elastic Net regression for predictive modeling.
- **Impact & Learnings**: My findings highlighted the significance of profile completeness and the number of pictures in attracting visitors. This project taught me the importance of clear, visual communication in profile engagement, and these insights could be used to inform the design of dating app algorithms and user interfaces to improve interaction rates and satisfaction.

### [Efficient Frontier](https://gitlab.oit.duke.edu/yl960/angelaliang_projects/-/blob/main/EfficientFrontier)

- **Description**: In this project, I used C++ to find optimizing multi-asset portfolio for each level of expeted return based on finding the optimal weights to achieve minimum volatility for each return
- **Language and Methods Used**: C++, Eigen package (MatrixXd)
- **Challenges & Learnings**: The key challenge of the project is to deal with the matrix calculation, and I chose to use the Eigen package as it is known for its high performance and optimized for speed. I thought deeply about corner cases, involving the actual numbers and matrices that could pose problems and tested these scenarios specifically.


### [Signature Methods in Machine Learning Research](https://gitlab.oit.duke.edu/yl960/angelaliang_projects/-/blob/main/signatures_method)

- **Description**: Signatures method is a novel approach for sequential learning and rooted in path thoery. Its basic concept is to represent multidimensional path by a graded feature set of their iterated integrals. It can be used to reduce dimensions of a large database and improve efficiency of machine learning models. In my research project, I wanted to study if there is any trade-offs between the improved efficiency brought by signatures method and accuracy of the results.  
- **Language and Package**: Python, signatures method 
- **Methods**: Using geometric Brownian motion simulator, I generated two classes of data to simulate real-world pricing data. The paths are transformed into their signatures and Random Forest is used to train and test the data. The accuracy rate is calculated and compared with the theoretical maximum. A Kaggle anomaly-detection dataset is used to demonstrate the practical application of the method. The goal is to predict the is-anomaly column and alert clients of any anomalies with data transformed by signature methods.  
- **Learnings**: Results show that there is a certain threshold for the signatures method to work accurately. Too small of a sample size might cause overfitting problem which limit the accuracy of signatures method.  

### [Multivariant and Regression](https://gitlab.oit.duke.edu/yl960/angelaliang_projects/-/blob/main/MultivariantAndRegression)
- **Description**: Engaged in statistical analyses and modeling, tackling problems in statistical package biasness, linear regression, and time series analysis. Refer to [`README.md`](https://gitlab.oit.duke.edu/yl960/angelaliang_projects/-/blob/main/MultivariantAndRegression/README.md) file for detail. 
- **Technologies Used**: Python, Pandas, NumPy, SciPy, Statsmodels
- **Key Accomplishments**:
      - **Problem 1**: Testing Statistical Package Biasness
      Conducted t-tests to evaluate the biasness in statistical packages.
      - **Problem 2**: Linear Regression Analysis
      Compared OLS and MLE methods for parameter estimation in linear regression.
      - **Problem 3**: Time Series Analysis
      Identified AR(3) as the best model for a given time series data set.
### More project updates coming up! 

## Skills

- Programming Languages: Python, C++, R, SQL 

## Contact

I'm continuously learning and I love talking with people from various industries to learn new things. Feel free to reach out to me for collaboration or if you have any questions or suggestions: 

- **Email**: angela.liang@duke.edu
- **LinkedIn**: https://www.linkedin.com/in/angelaliang2019/

Thank you for visiting my project portfolio!
