## Description ##
In the Curiosity Cup 2024, my team examined the intricacies of dating profile effectiveness on the Lovoo platform. As part of the team, I focused on finding the elements that contribute to a user's success in attracting profile visits and engagement. My analysis revolved around a dataset from Kaggle, featuring 4015 female profiles, with a focus on quantifiable metrics such as profile visits, fan counts, and 'kisses'. The dataset utilized for this project is included in the `profile_dataset` folder.

## Project Structure
- `Dating App Study Report.pdf`: A report detailing the study's objectives, methodology, results, and potential impacts on the dating app industry.

## Methodology
This project entailed a systematic approach to understanding dating profile attractiveness:
1. **Data Cleansing**: Employing SAS to clean and prepare the dataset for analysis.
2. **Exploratory Analysis**: Investigating underlying patterns and correlations within the data.
3. **Predictive Modeling**: Utilizing Elastic Net regression to evaluate the importance of various profile features.

## Results and Findings
The study revealed that profiles with a greater number of pictures and more comprehensive information significantly increase profile visit rates. The detailed findings and their implications for enhancing user engagement on dating platforms are discussed in the presentation.

## Contact
If you have questions or would like to discuss the project further, please don't hesitate to reach out at angela.liang@duke.edu.
